#ifndef BLUR_TASK_H
#define BLUR_TASK_H

#include <vector>
#include <thread>
#include "cimg.h"
#include "pixel-map.h"

class BlurTask {
  public:
  typedef unsigned int BaseUnit;
  typedef cimg_library::CImg<BaseUnit> Image;
  BlurTask(PixelMap *_pm,  Image *_img, int _start, int _len, int _r, bool sync = false);
  std::vector<BaseUnit> GetResult();
  private:
  int start;
  int len;
  int r;
  PixelMap *pm;
  std::thread *task;
  std::vector<BaseUnit> result;
  Image *img;
  BaseUnit GetPixel(int x, int y);
  void Run();
  void RunDetached();
  void Join();
  unsigned int ComputeGaussian(int x, int y);
};

#endif
