#include <getopt.h>
#include <libgen.h>
#include <functional>
#include "blur.h"
#include "log.h"

using namespace cimg_library;
using BMP = CImg<unsigned int>;
using namespace std;

char *base;
char *output_filename = 0;
char *input_filename = 0;
char is_verbose = 0;
unsigned int radius = 0;

BMP *DoBlur(BMP *input, unsigned int threads) {
  vector<BlurTask> tasks;
  PixelMap pm(input->width(), input->height());
  unsigned int total = input->width() * input->height();
  unsigned int start = 0;
  unsigned int len = ceil((double) total / threads);
  for (unsigned int i = 0; i < threads - 1; ++i) {
    tasks.push_back(BlurTask(&pm, input, start, min(len, total - start), radius));
    start += len;
  }
  BlurTask sync(&pm, input, start, min(len, total - start), radius, true);
  PixelMap::Incrementer inc = pm.MakeIncrementer();
  int x = 0, y = 0;
  for (auto it = tasks.begin(); it != tasks.end(); it++) {
    for (auto res = it->GetResult().begin(); res != it->GetResult().end(); res++) {
      input->operator()(x, y) = *res;
      inc(&x, &y);
    }
  }
  for (auto res = sync.GetResult().begin(); res != sync.GetResult().end(); res++) {
    input->operator()(x, y) = *res;
    inc(&x, &y);
  }
  return input;
}

BMP *DoBlur(BMP *input) {
  return DoBlur(input, thread::hardware_concurrency());
}

void DrawToScreen(BMP *input, const char *filename) {
  CImgDisplay screen(*input, filename);
  while (!screen.is_closed()) {
    screen.wait_all();
  }
}

int main(int argc, char **argv) {
  static struct option long_options[] = {
    {"verbose", no_argument, 0, 'v'},
    {"radius", required_argument, 0, 'r'},
    {"output", required_argument, 0, 'o'},
    {0, 0, 0, 0}
  };
  int c, long_index = 0;
  base = basename(argv[0]);
  while ((c = getopt_long(argc, argv, "vo:", long_options, &long_index)) != -1) {
    switch (c) {
      case 'v':
        is_verbose = 1;
        break;
      case 'o':
        output_filename = optarg;
        break;
      case '?':
        if (optopt == 'o') {
          Die("option -%c requires an argument", optopt);
        } else if (isprint(optopt)) {
          Die("unknown option '-%c'. Run '%s --help' to see options'", optopt, base);
        } else {
          Die("unknown option character '\\x%x'. Run '%s --help' to see options'", optopt, base);
        }
        break;
      case 'r':
        radius = atoi(optarg);
	break;
    }
  }
  while (optind < argc) {
    input_filename = argv[optind];
    ++optind;
  }
  if (!input_filename) Die("requires input filename");
  BMP *img = new BMP();
  try {
    img->load_bmp(input_filename);
  } catch (exception _) {
    Die("failed to load %s", input_filename);
  }
  BMP *blurred_image = DoBlur(img);
  if (!output_filename) DrawToScreen(blurred_image, input_filename);
  else {
    try {
      blurred_image->save_bmp(output_filename);
      Log("image saved to %s", output_filename);
    } catch (exception _) {
      Die("failed to save image to %s", output_filename);
    }
  }
  return 0;
}
