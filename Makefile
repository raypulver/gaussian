CXX = g++
SOURCES = blur.cpp task.cpp pixel-map.cpp
FLAGS = -std=c++11 -lX11 -lpthread -I/usr/X11/include -L/usr/X11/lib
OUTPUT = blur

all: $(SOURCES)
	$(CXX) -o $(OUTPUT) $(SOURCES) $(FLAGS)
