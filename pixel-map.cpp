#include "pixel-map.h"

PixelMap::PixelMap(int _width, int _height) : width(_width), height(_height) {}

int PixelMap::To1D(int w, int h) {
    return h*width + w;
}
void PixelMap::From1D(int r, int *w, int *h) {
  *h = r / width;
  *w = r % width;
}
PixelMap::Incrementer PixelMap::MakeIncrementer() {
  return [=] (int *x, int *y) -> bool {
    if (*x >= width) {
      if (*y >= height) return false;
      ++(*y);
    } else {
      ++(*x);
    }
    return true;
  };
}

int PixelMap::GetWidth() { return width; }
int PixelMap::GetHeight() { return height; }


