#include "task.h"
#include <thread>
#include <functional>
#include <vector>

using namespace std;

vector<BlurTask::BaseUnit> BlurTask::GetResult() {
  return result;
}

BlurTask::BlurTask(PixelMap *_pm,  Image *_img, int _start, int _len, int _r, bool sync) : pm(_pm), img(_img), start(_start), len(_len), r(_r) {
  if (sync) Run();
  else RunDetached();
}

void BlurTask::Join() {
  if (task) {
    task->join();
    delete task;
  }
}

unsigned int BlurTask::GetPixel(int x, int y) {
  return img->operator()(x, y);
}

void BlurTask::Run() {
  int x, y;
  for (unsigned i = start; i < start + len; ++i) {
    pm->From1D(i, &x, &y);
    result.push_back(ComputeGaussian(x, y));
  }
}

void BlurTask::RunDetached() {
  task = new std::thread(&BlurTask::Run, this);
}

unsigned int BlurTask::ComputeGaussian(int x, int y) {
  unsigned int pxl = GetPixel(x, y);
  double a = pxl & 0xff000000;
  double r = 0, g = 0, b = 0;
  for (unsigned i = 0; i < img->width(); ++i) {
    for (unsigned j = 0; j < img->height(); ++j) {
      unsigned char inner_pxl = GetPixel(i, j);
      unsigned char inner_r = (pxl & 0x00ff) >> 8;
      unsigned char inner_g = (pxl & 0x0000ff) >> 16;
      unsigned char inner_b = (pxl & 0x000000ff) >> 24;
      double coefficient = exp(-(pow(x - floor((double) img->width()/2), 2) + pow(y - floor((double) img->height()/2), 2))/(2*pow(r, 2)))*(1.0/(2*M_PI*pow(r, 2)));
      r += coefficient*inner_r;
      g += coefficient*inner_g;
      b += coefficient*inner_b;
    }
  }
  return (unsigned int) a | ((unsigned int) r << 8) | ((unsigned int) g << 16) | ((unsigned int) b << 24);
}
