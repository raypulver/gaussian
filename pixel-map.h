#ifndef PIXEL_MAP_H
#define PIXEL_MAP_H

#include <functional>

class PixelMap {
  int width;
  int height;
  public:
  typedef std::function<bool(int *, int *)> Incrementer;
  int GetWidth();
  int GetHeight();
  PixelMap(int _width, int _height);
  int To1D(int w, int h);
  void From1D(int r, int *w, int *h);
  Incrementer MakeIncrementer();
};

#endif
