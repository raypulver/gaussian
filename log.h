#ifndef LOG_H
#define LOG_H

#include <cstring>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include "blur.h"

#define MAX_FORMAT_STRING_SIZE (1 << 16)

using namespace std;

template <typename T, typename... Args> void Log(T fmt, Args ...args) {
  char buf[MAX_FORMAT_STRING_SIZE];
  memset(buf, 0, sizeof(buf));
  sprintf(buf, fmt, args...);
  string err = base;
  err += ": ";
  err += buf;
  cerr << err << endl;
}

template <typename T> void Log(T arg) {
  Log("%s", arg);
}

template <typename... Args> void Die(Args ...args) {
  Log(args...);
  exit(1);
}

#endif
